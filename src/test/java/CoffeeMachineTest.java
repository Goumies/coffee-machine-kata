import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import mockito.JUnitPlatform;
import mockito.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class CoffeeMachineTest {

	private CoffeeMachine coffeeMachine = new CoffeeMachine();
	private OrderFromCustomer orderFromCustomerMock = new OrderFromCustomer(CoffeeMachine.Drink.T);
	
	@Test
	public void shouldSendErrorMessageForIncorrectInstructionsForDrinks() {
		assertEquals("Error : Invalid input", coffeeMachine.selectDrink(""));
	}
	
	@Test
	public void shouldReturnTForTea() {
		assertEquals("T", coffeeMachine.selectDrink("T"));
	}
	
	@Test
	public void shouldReturnHForChocolate() {
		assertEquals("H", coffeeMachine.selectDrink("H"));
	}
	
	@Test
	public void shouldReturnCForCoffee() {
		assertEquals("C", coffeeMachine.selectDrink("C"));
	}

	@Test
	public void shouldReturn1ToAdd1sugar() {
		assertEquals("1", coffeeMachine.addSugar("1"));
	}
	
	@Test
	public void shouldReturn2ToAdd2sugars() {
		assertEquals("2", coffeeMachine.addSugar("2"));
	}
	
	@Test
	public void shouldReturnTAndColonAndColonWhenOrderingTeaWithoutSugar() {
		orderFromCustomerMock = new OrderFromCustomer(CoffeeMachine.Drink.T);
		assertEquals("T::", coffeeMachine.generateInstructionsForDrinkMaker(orderFromCustomerMock));
	}
	
	@Test
	public void shouldReturnTAndColonAnd1AndColonAnd0WhenOrderingTeaWith1Sugar() {
		orderFromCustomerMock = new OrderFromCustomer(CoffeeMachine.Drink.T, 1);
		assertEquals("T:1:0", coffeeMachine.generateInstructionsForDrinkMaker(orderFromCustomerMock));
	}
	
	@Test
	public void shouldReturnHAndColonAndColonWhenOrderingChocolateWithNoSugarAndNoStick() {
		orderFromCustomerMock = new OrderFromCustomer(CoffeeMachine.Drink.H);
		assertEquals("H::", coffeeMachine.generateInstructionsForDrinkMaker(orderFromCustomerMock));
	}
	
	@Test
	public void shouldReturnTAndColonAnd2AndColonAnd0WhenOrderingCoffeeWith2Sugars() {
		orderFromCustomerMock = new OrderFromCustomer(CoffeeMachine.Drink.C, 2);
		assertEquals("C:2:0", coffeeMachine.generateInstructionsForDrinkMaker(orderFromCustomerMock));
	}
	
	@Test
	public void shouldReturnMAndMessageContentWhenSendingMessageToTheCoffeeMachineInterface() {
		assertEquals("M:message-content", coffeeMachine.generateMessageForCoffeeMachineInterface("message-content"));
	}
	
	@Test
	public void shouldReturnTrueWhenOrderIsGivenTheCorrectAmountForATea() {
		assertEquals(true, coffeeMachine.isGivenAmountEnough("T", new BigDecimal("0.4")));
	}
	
	@Test
	public void shouldReturnTrueWhenOrderIsGivenTheCorrectAmountForAChocolate() {
		assertEquals(true, coffeeMachine.isGivenAmountEnough("H", new BigDecimal("0.5")));
	}
	
	@Test
	public void shouldReturnTrueWhenOrderIsGivenTheCorrectAmountForACoffee() {
		assertEquals(true, coffeeMachine.isGivenAmountEnough("C", new BigDecimal("0.6")));
	}
	
	@Test
	public void shouldReturnTAndColonAnd1AndColonAnd0WhenGivenOrderWith40Cents() {
		orderFromCustomerMock = new OrderFromCustomer(CoffeeMachine.Drink.T, 1);
		orderFromCustomerMock = orderFromCustomerMock.updateCurrentAmount(new BigDecimal("0.4"));
		assertEquals("T:1:0", coffeeMachine.sendToDrinkMaker(orderFromCustomerMock));
	}
	
	@Test
	public void shouldReturnCAndColonAnd2AndColonAnd0WhenGivenOrderWith1Euro() {
		orderFromCustomerMock = new OrderFromCustomer(CoffeeMachine.Drink.C, 2);
		orderFromCustomerMock = orderFromCustomerMock.updateCurrentAmount(new BigDecimal("1"));
		assertEquals("C:2:0", coffeeMachine.sendToDrinkMaker(orderFromCustomerMock));
	}
	
	@Test
	public void shouldReturnMAndColonMessageContentWhenOrderIsMissing10Cents() {
		orderFromCustomerMock = new OrderFromCustomer(CoffeeMachine.Drink.H);
		orderFromCustomerMock = orderFromCustomerMock.updateCurrentAmount(new BigDecimal("0.4"));
		assertEquals("M:Missing 10 cents to complete order", coffeeMachine.sendToDrinkMaker(orderFromCustomerMock));
	}
	
	@Test
	public void shouldReturnOAndColonAndColonWhenGivenOrderWith60Cents() {
		orderFromCustomerMock = new OrderFromCustomer(CoffeeMachine.Drink.O);
		orderFromCustomerMock = orderFromCustomerMock.updateCurrentAmount(new BigDecimal("0.6"));
		assertEquals("O::", coffeeMachine.sendToDrinkMaker(orderFromCustomerMock));
	}
	
	@Test
	public void shouldReturnChAndColonAndColonWhenOrderingExtraHotCoffeeWithoutSugar() {
		orderFromCustomerMock = new OrderFromCustomer(CoffeeMachine.Drink.C);
		orderFromCustomerMock = orderFromCustomerMock.updateCurrentAmount(new BigDecimal("1"));
		orderFromCustomerMock = orderFromCustomerMock.updateOption(CoffeeMachine.Option.h);
		assertEquals("Ch::", coffeeMachine.sendToDrinkMaker(orderFromCustomerMock));
	}
	
	@Test
	public void shouldReturnHhAndColonAnd1AndColonAnd0WhenOrderingExtraHotChocolateWith1Sugar() {
		orderFromCustomerMock = new OrderFromCustomer(CoffeeMachine.Drink.H, 1);
		orderFromCustomerMock = orderFromCustomerMock.updateCurrentAmount(new BigDecimal("1"));
		orderFromCustomerMock = orderFromCustomerMock.updateOption(CoffeeMachine.Option.h);
		assertEquals("Hh:1:0", coffeeMachine.sendToDrinkMaker(orderFromCustomerMock));
	}
	
	@Test
	public void shouldReturnThAndColonAnd2AndColonAnd0WhenOrdereringExtraHotTeaWith2Sugars() {
		orderFromCustomerMock = new OrderFromCustomer(CoffeeMachine.Drink.T, 2);
		orderFromCustomerMock = orderFromCustomerMock.updateCurrentAmount(new BigDecimal("1"));
		orderFromCustomerMock = orderFromCustomerMock.updateOption(CoffeeMachine.Option.h);
		assertEquals("Th:2:0", coffeeMachine.sendToDrinkMaker(orderFromCustomerMock));
	}
}
