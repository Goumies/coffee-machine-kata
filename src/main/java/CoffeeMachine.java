import java.math.BigDecimal;

public class CoffeeMachine {

	public enum Drink {
		T (new BigDecimal("0.4"), "T"), 
		H (new BigDecimal("0.5"), "H"), 
		C (new BigDecimal("0.6"), "C"),
		O (new BigDecimal("0.6"), "O");
		
		private final BigDecimal price;
		private final String drinkLetter;
		
		Drink(BigDecimal price, String drinkLetter) {
			this.price = price;
			this.drinkLetter = drinkLetter;
		}
		
		public BigDecimal price() {
			return price;
		}
		
		public String drinkLetter() {
			return drinkLetter;
		}
	}
	
	public enum Option {
		h
	}
	
	public String selectDrink(String drink) {
		String result = "";
		if (!drink.equalsIgnoreCase(Drink.T.toString()) 
				&& !drink.equalsIgnoreCase(Drink.H.toString())
				&& !drink.equalsIgnoreCase(Drink.C.toString())
				&& !drink.equalsIgnoreCase(Drink.O.toString()))
			result = "Error : Invalid input";
		else 
			result = drink;
		
		return result;
	}

	public String addSugar(String numberOfSugars) {
		if(numberOfSugars.equals("1") || numberOfSugars.equals("2")) {
			return numberOfSugars;
		} else {
			return "";
		}
	}

	public String generateInstructionsForDrinkMaker(OrderFromCustomer orderFromCustomer) {
		String instructions ="";
		if (orderFromCustomer.drinkToString().equals(Drink.T.toString())
				|| orderFromCustomer.drinkToString().equals(Drink.H.toString())
				|| orderFromCustomer.drinkToString().equals(Drink.C.toString())
				|| orderFromCustomer.drinkToString().equals(Drink.O.toString()))
			instructions += selectDrink(orderFromCustomer.drinkToString());
			if(orderFromCustomer.optionToString().equals(Option.h.toString()))
				instructions += "h";
		instructions += ":";
		if (orderFromCustomer.sugars().isEmpty())
			instructions += ":";
		else
			instructions += addSugar(orderFromCustomer.sugars()) + ":0";
		return instructions;
	}

	public String generateMessageForCoffeeMachineInterface(String messageContent) {
		return "M:" + messageContent;
	}

	public Boolean isGivenAmountEnough(String selectedDrink, BigDecimal givenAmountFromCustomer) {
		Boolean result = false;
		if(selectedDrink != null) {
			result = isGivenAmountCorrespondingToSelectedDrink(selectedDrink, givenAmountFromCustomer);
		};
		return result;	
	}

	private Boolean isGivenAmountCorrespondingToSelectedDrink(String selectedDrink, BigDecimal givenAmountFromCustomer) {
		Boolean result = false;
		if(selectedDrink.equalsIgnoreCase(Drink.T.drinkLetter()) && (givenAmountFromCustomer.compareTo(Drink.T.price()) == 0 || givenAmountFromCustomer.compareTo(Drink.T.price()) == 1))
			result = true;
		if(selectedDrink.equalsIgnoreCase(Drink.H.drinkLetter()) && (givenAmountFromCustomer.compareTo(Drink.H.price()) == 0
				|| givenAmountFromCustomer.compareTo(Drink.H.price()) == 1))
			result = true;
		if(selectedDrink.equalsIgnoreCase(Drink.C.drinkLetter()) && (givenAmountFromCustomer.compareTo(Drink.C.price()) == 0
				|| givenAmountFromCustomer.compareTo(Drink.C.price()) == 1))
			result = true;
		if(selectedDrink.equalsIgnoreCase(Drink.O.drinkLetter()) && (givenAmountFromCustomer.compareTo(Drink.O.price()) == 0
				|| givenAmountFromCustomer.compareTo(Drink.O.price()) == 1))
			result = true;
		return result;
	}

	public String sendToDrinkMaker(OrderFromCustomer orderFromCustomer) {
		String result = "";
		if(orderFromCustomer != null) {
			if(isGivenAmountEnough(orderFromCustomer.drinkToString(), orderFromCustomer.currentAmount())) {
				if(isGivenAmountCorrespondingToSelectedDrink(orderFromCustomer.drinkToString(), orderFromCustomer.currentAmount()))
					result = generateInstructionsForDrinkMaker(orderFromCustomer);
			} else
				result =  generateMessageForCoffeeMachineInterface(generateMissingAmountMessage(orderFromCustomer.drinkConstant(), orderFromCustomer.currentAmount()));
		}
		
		return result;
	}

	private String generateMissingAmountMessage(CoffeeMachine.Drink selectedDrink, BigDecimal orderCurrentAmount) {
		if(orderCurrentAmount.compareTo(new BigDecimal("1")) == -1)
			return "Missing " + formatMissingAmount(selectedDrink, orderCurrentAmount) + " cents to complete order";
		else
			return "Missing " + formatMissingAmount(selectedDrink, orderCurrentAmount) + " euros to complete order";
	}

	private int formatMissingAmount(CoffeeMachine.Drink selectedDrink, BigDecimal orderCurrentAmount) {
		if(orderCurrentAmount.compareTo(new BigDecimal("1")) == -1)
			return ((selectedDrink.price().subtract(orderCurrentAmount)).multiply(new BigDecimal("100"))).intValueExact();
		else
			return selectedDrink.price().subtract(orderCurrentAmount).intValueExact();
	}
	
	

}
