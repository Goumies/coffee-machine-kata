import java.math.BigDecimal;

public class OrderFromCustomer {
	private CoffeeMachine.Drink drink;
	private String sugars;
	private BigDecimal currentAmount;
	private CoffeeMachine.Option option;
	
	public OrderFromCustomer(CoffeeMachine.Drink drink) {
		this.drink = drink;
		this.sugars = "";
	}
	
	public OrderFromCustomer(CoffeeMachine.Drink drink, int sugars) {
		this.drink = drink;
		this.sugars = String.valueOf(sugars);
	}
	
	public OrderFromCustomer(CoffeeMachine.Drink drink, String sugars, BigDecimal currentAmount) {
		this.drink = drink;
		this.sugars = String.valueOf(sugars);
		this.currentAmount = currentAmount;
	}
	
	public OrderFromCustomer(CoffeeMachine.Drink drink, String sugars, BigDecimal currentAmount, CoffeeMachine.Option option) {
		this.drink = drink;
		this.sugars = String.valueOf(sugars);
		this.currentAmount = currentAmount;
		this.option = option;
	}
	
	public OrderFromCustomer updateCurrentAmount(BigDecimal currentAmount) {
		return new OrderFromCustomer(drink, sugars, currentAmount);
	}
	
	public OrderFromCustomer updateOption(CoffeeMachine.Option option) {
		return new OrderFromCustomer(drink, sugars, currentAmount, option);
	}
	
	public String drinkToString() {
		return String.valueOf(drink);
	}
	
	public String optionToString() {
		return String.valueOf(option);
	}
	
	public CoffeeMachine.Drink drinkConstant() {
		return this.drink;
	}
	
	public String sugars() {
		return this.sugars;
	}
	
	public BigDecimal currentAmount() {
		return this.currentAmount;
	}
	
	public CoffeeMachine.Option option() {
		return this.option;
	}
}
